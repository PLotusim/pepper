from naoqi import ALProxy
motion = ALProxy("ALMotion", "192.168.0.115", 9559)
#Pepper will not move if the stiffness of the joints to something that is not 0
motion.setStiffnesses("Body", 0)
motion.moveInit()
motion.post.moveTo(0.1, 0, 0)
