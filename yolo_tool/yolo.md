# YOLO环境配置以及训练方法
## 环境配置：<br />
`ubuntu-16`<br />
`cuda-10.0`<br />
`cudnn-7.6`<br />
`yolov4`<br />
`opencv-3.4.16`<br />
注意：版本最好一致，不然会出现奇怪的bug<br />

## nvidia+cuda+cudnn+opencv
以下是基于ubuntu16的环境配置方法
1. 验证是否安装驱动 `nvidia-smi` <br />
[nvidia驱动安装](https://blog.csdn.net/weixin_34910922/article/details/118076747) <br />
2. 下载cuda前请先查看[cuda版本与驱动对应情况](https://blog.csdn.net/luanfenlian0992/article/details/108303944) <br />
[cuda安装](https://blog.csdn.net/sinat_36721621/article/details/115326307)
3. [cudnn安装](https://blog.csdn.net/public669/article/details/98470857)(注意cudnn要与cuda版本对应) <br />
4. [下载opencv](https://blog.csdn.net/u013066730/article/details/79411767)
5. `备注`：[删除cuda](https://blog.csdn.net/weixin_44711603/article/details/110233047)
   
## 部署yolov4
1. [下载darknet和make](https://blog.csdn.net/weixin_39298885/article/details/111690284)


## VOC数据集制作
1. [安装labelimg](https://blog.csdn.net/python_pycharm/article/details/85338801)<br />
[labelimg使用教程](https://blog.csdn.net/Thebest_jack/article/details/124260693)<br />
`备注`：因为ubuntu16使用labelimg受限python版本，所以采用windows环境下标注图片
2. 重写图片。运行`convert.py`代码，重写图片，需要修改`dir_origin_path`和`dir_save_path`，分别为当前图片存储的绝对路径，以及重写后图片的保存路径。
3. 重命名图片。运行`rename.py`代码，重命名图片，需要修改`image_dir`和`output_dir`。<br />
`备注`:运行前最好将图片备份，因为运行后原文件目录下的图片会被删除，只剩下output_dir的重命名图片。
4. 使用labelimg标注，生成xml文件。
5. 修改xml文件的path路径。因为标注的图片路径和训练下的图片路径不一致，需要修改xml里面的path路径。运行`xml_c.py`代码，修改路径，需要修改`path0.firstChild.data`
6. 进行[训练yolov4](https://blog.csdn.net/weixin_39298885/article/details/111690284)的第二部分
   