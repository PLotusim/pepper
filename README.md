## pepper环境配置
1. 配置ubuntu14或16环境<br />
2. 保存文件下载python_SDK[下载地址](https://developer.softbankrobotics.com/pepper-naoqi-25-downloads-linux)  解压到**home目录**,修改文件名为pynaoqi<br />
3. 添加naoqi至环境变量，具体流程如下:
    * 终端输入`sudo gedit ~/.bashrc`
    * 在文件最末尾加入`PYTHONPATH=~/pynaoqi/lib/python2.7/site-packages`
    * 返回终端,输入`source ~/.bashrc`
    * 验证，终端输入`python`,然后`import naoqi`和`import qi`,如果没有报错，就说明下载成功

## 将代码拉到本地
实现过程详看[帮助文档](https://gitee.com/help/articles/4122)<br />
可以使用`test`代码连接pepper，注意修改py文件里面的IP地址<br />
**IP地址获取方法**：按一下pepper平板下的按钮，它会报IP地址，一般为192.168.0.111<br />
**连接失败**：查看是否连接正确的网络，即WHUAI的wifi
## Choregraphe环境配置
1. 去[Choregraphe下载地址](https://developer.softbankrobotics.com.cn/tools)下载安装包，保存文件
2. 解压，然后终端（`ctrl+alt+T`）打开`cd choregraphe-suite-2.5.11.13-linux64`，进入到`cd bin`目录下，输入`./choregraphe-bin`(输入`./cho`后可以用`tab`补全)
3. 会弹出一个输入激活码窗口，而激活码在下载网页上显示
4. 进入到Choregraphe界面，即安装成功
5. 以后使用Choregraphe，详见第二条
## linux相关知识(持续补充中...)
## ubuntu16 pip升级
(解决ubuntu16.04 you are using pip version 8.1.1,however version 20.3.4 is available问题)
1. `sudo apt-get update`
2. `sudo apt-get upgrade`
3. `wget https://bootstrap.pypa.io/pip/2.7/get-pip.py`
4. `sudo python get-pip.py`
## 官方资料
[Python SDK Tutorials](https://developer.softbankrobotics.com/pepper-naoqi-25/naoqi-developer-guide/other-tutorials/python-sdk-tutorials)<br />
[NAOqi APIs](https://developer.softbankrobotics.com/pepper-naoqi-25/naoqi-developer-guide/naoqi-apis)

    



