# Carry my luggage

## Setp
### No1.识别物体
**主要流程**：pepper机器人通过视觉观察目标前的物体，说出人手指的物品的位置（即左右）<br />
**难点**：手势加物体识别 <br />
**预计解决方法**：尝试将手势与物体一起作为训练材料进行训练，记录效果<br />
[代码链接 Vision Recongition](https://developer.softbankrobotics.com/pepper-naoqi-25/naoqi-developer-guide/other-tutorials/python-sdk-tutorials/python-sdk-examples-5-2)

### No2.语音识别
**主要流程**:识别跟随命令，进入到第三阶段<br />
**难点**：命令的准确识别以及如何进入到跟踪阶段<br />
**预计解决方法**：先尝试官方的API调用，并使用状态机进行转换<br />

### No3.跟踪目标行动
**主要流程**：pepper机器人根据人并跟随 <br/>
**难点**：人体或者人脸识别，并进行跟随<br/>
**预计解决方法**:官网有现成的代码，在它的基础上进行改进<br />
[代码链接 FaceDetection and Tracking](https://developer.softbankrobotics.com/pepper-naoqi-25/naoqi-developer-guide/other-tutorials/python-sdk-tutorials/python-sdk-examples-5-3)

